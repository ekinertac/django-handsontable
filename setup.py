import os
from setuptools import setup
from handsontable import VERSION

f = open(os.path.join(os.path.dirname(__file__), 'README.md'))
readme = f.read()
f.close()

setup(
    name='django-handsontable',
    version=".".join(map(str, VERSION)),
    description='This reusable Django app using Handsontable.js',
    long_description=readme,
    author="Ekin Ertac",
    author_email='ekinertac@gmail.com',
    url='https://github.com/ekinertac/django-handsontable',
    packages=['handsontable'],
    include_package_data=True,
    install_requires=['setuptools'],
    zip_safe=False,
    classifiers=[
    'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
)