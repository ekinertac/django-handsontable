# django-handsontable
Django admin integrated JavaScript Spreadsheet

### Screenshot


### What's that
django-handsontable is a reusable application for Django, using Handsontable spreadsheets in Django Admin https://handsontable.com/

### Dependecies
 - Django >= 1.7

### Installing

- Install django-handsontable

```
pip install django-handsontable
```

- Add `'handsontable', `  to `INSTALLED_APPS`
- Voila!

### Using in Model

```python

from django.db import models
from handsontable.fields import SpreadSheetField


class Entry(models.Model):
    title = models.CharField(max_length=250, verbose_name=u'Title')
    sheet = SpreadSheetField()
```

or use custom options:

```python
    sheet = SpreadSheetField(
        options={
            'minRows': 30,
            'minCols': 26,
        }
    )
```



