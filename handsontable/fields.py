from django.db.models import Field
from handsontable.widgets import SpreadSheetEditor


class SpreadSheetField(Field):
    def __init__(self, *args, **kwargs):
        options = kwargs.pop('options', {})

        before_render = kwargs.pop('before_render', "")
        after_render = kwargs.pop('after_render', "")

        self.widget = SpreadSheetEditor()
        super(SpreadSheetField, self).__init__(*args, **kwargs)

    def get_internal_type(self):
        return "TextField"

    def formfield(self, **kwargs):
        defaults = {'widget': self.widget}
        defaults.update(kwargs)
        return super(SpreadSheetField, self).formfield(**defaults)