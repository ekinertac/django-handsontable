import json

from django.forms import widgets
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse

from django.conf import settings


GLOBAL_OPTIONS = getattr(settings, 'HANDSONTABLE_OPTIONS', {
    'height': 300,
    'minRows': 30,
    'minCols': 26,

    'rowHeaders': True,
    'colHeaders': True,

    'stretchH': 'all',

    'autoWrapRow': True,
    'columnSorting': True,
    'filters': True,
    'dropdownMenu': True,
    'contextMenu': True,
    'autoRowSize': True,
    'manualColumnMove': True,
    'manualRowMove': True,
    'fillHandle': {
        'autoInsertRow': True,
    },

})

INIT_JS = """
<style>
    #%s {
        display: none;
        font-family: monospace;
    }
    .toggle-wrapper {
        text-align: right;
        padding: 10px 0;
    }
</style>

<script type="text/javascript">
    var id = '%s';
    var $ = django.jQuery;

    $(document).ready(function(){
        var textarea = $('#' + id);

        textarea.before('<div class="toggle-wrapper"><a href="#" class="toggle-data">Toggle Data/Sheet</a></div>');

        var data = JSON.parse(textarea.val()) || [["", ""]];

        textarea.after("<div id='spreadsheet-" + id + "'>");

        var container = document.getElementById('spreadsheet-' + id);
        var options = %s;
        options.data = data;

        // Before Render. Function definitons etc.
        %s

        // Render Spreadsheet
        var hot = new Handsontable(container, options);

        
        // After Render. Events etc.
        var afterCellChange = function(change, soruce){
            textarea.val(JSON.stringify(hot.getData()));
        }

        Handsontable.hooks.add('afterChange', afterCellChange, hot);

        %s

        $('.toggle-wrapper').on('click', '.toggle-data', function(e){
            e.preventDefault();
            if(textarea.css('display') == 'none') {
                textarea.show();
                $('.handsontable').hide()
            }else{
                textarea.hide();
                $('.handsontable').show()
            }
            
        })
    });

</script>
"""


class SpreadSheetEditor(widgets.Textarea):

    class Media:
        css = {
            "all": (
                "handsontable/css/handsontable.min.css",
            )
        }

        js = (
            "handsontable/js/handsontable.full.js",
        )

    def __init__(self, *args, **kwargs):
        self.custom_options = kwargs.pop('options', {})

        self.before_render = kwargs.pop('before_render', "")
        self.after_render = kwargs.pop('after_render', "")

        super(SpreadSheetEditor, self).__init__(*args, **kwargs)

    def get_options(self):
        options = GLOBAL_OPTIONS.copy()
        options.update(self.custom_options)
        options.update({})
        return json.dumps(options)

    def render(self, name, value, attrs=None):
        html = super(SpreadSheetEditor, self).render(name, value, attrs)
        final_attrs = self.build_attrs(attrs)
        id_ = final_attrs.get('id')
        html += INIT_JS % (id_, id_, self.get_options(), self.before_render, self.after_render)
        return mark_safe(html)
